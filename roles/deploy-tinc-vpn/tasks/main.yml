---

- name: Install tinc
  apt: 
    name:  tinc
    state: present
    update_cache: yes

- name: Create directory if it doesn't exist
  file:
    path:  "{{ tinc_dir_hosts }}"
    state: directory

- name: Generate "tinc.conf"
  template: 
    src:  tinc.conf.j2
    dest: "{{ tinc_dir }}/tinc.conf"
    mode: 0755

- name: Check tinc private key
  stat:
    path: "{{ tinc_private_key }}"
  register: check_tinc_private_key

- name: Check tinc public key
  stat:
    path: "{{ tinc_dir_hosts }}/{{ hostname }}"
  register: check_tinc_public_key

- name: Generate RSA keys 
  shell:
    cmd: "tincd -n {{ tinc_net_name }} -K4096 > /dev/null"
  register: output_generate_rsa_1
  when: not check_tinc_private_key.stat.exists or not check_tinc_public_key.stat.exists

- name: Generate "tinc-up"
  template:
    src:  tinc-up.j2
    dest: "{{ tinc_dir }}/tinc-up"
    mode: 0755

- name: Generate "tinc-down"
  template:
    src:  tinc-down.j2
    dest: "{{ tinc_dir }}/tinc-down"
    mode: 0755

- name: Fetch server public key to client
  slurp: 
    src: "{{ tinc_dir_hosts }}/server"
  register: "server_key"
  delegate_to: server

- name: Put client public key to server
  copy:
    content: "{{ server_key.content | b64decode }}"
    dest: "{{ tinc_dir_hosts }}/server"
  notify:
    - Restart service tinc@ on client

- name: Fetch client public key to server
  slurp: 
    src: "{{ tinc_dir_hosts }}/{{ hostname }}"
  register: "client_key"

- name: Put client public key to server
  copy:
    content: "{{ client_key.content | b64decode }}"
    dest: "{{ tinc_dir_hosts }}/{{ hostname }}"
  delegate_to: server
  notify:
    - Restart service tinc@ on server

- name: Start service tinc@
  service:
    name: "{{ service_tinc_name }}"
    state: started
    enabled: yes