**Требования:**
- Пользователь с правами **sudo**
- Объявленные переменные в каждом **roles/<role_name>/vars/main.yml**
	- `ansible_user: <USER>` 
	- `ansible_password: <PASSWORD>`
# Список ролей

- **deploy-tinc-vpn** - установка и настройка клиентов tinc
- **update-packages** - обновление пакетов в системе
- **create-user** - создание пользователя 
- **delete-user** - удаление пользователя

## Роль deploy-tinc-vpn
Установка пакета и конфигурирование  **tinc-client**.
Для корректной работы плейбука требуется заранее настроенный и готовый к работе **tinc-server**.

Доступные для использования переменные в **roles/deploy-tinc-vpn/vars/main.yml**:

- **route** - Маршрут. Используется при генерации скриптов:
	-  templates/tinc-up
	-  templates/tinc-down
- **tinc_dir** - Корневая директория tinc. Используется для указания пути к конфигурационным файлам:
	- tasks/main.yml
- **tinc_dir_hosts** - Директория с публичными ключами хостов. Используется для создания структуры директорий:
	- tasks/main.yml
- **tinc_private_key** - Путь до приватного ключа. Используется для проверки существования приватного ключа и запуска задания на генерацию пары ключей в случае их отсутствия:
	- tasks/main.yml
- **service_tinc_name** - Название Systemd юнита. Используется для первого запуска службы tinc, а также для рестарта службы в случае если были обнаружены новые клиенты:
	- tasks/main.yml
	- handlers/main.yml
- **tinc_net_name** - Название сети tinc. Используется при генерации ключей в случае их отсутствия:
	- tasks/main.yml

Дополнительные переменные, которые необходимо указать в inventory файле **hosts**:

- **hostname** - Имя хоста. Используется при генерации конфигурационного файла:
	-  templates/tinc.conf 
- **vpn_ip** - IP-адрес в сети VPN. Используется при генерации скриптов:
	-  templates/tinc-up
	-  templates/tinc-down

## Роль update-packages

Обновление всех пакетов в системе.
В данный момент не требует дополнительных переменных.

## Роль create-user

Создание новой пользовательской учетной записи.
Доступные для использования переменные в **roles/create-user/vars/main.yml**:

- **new_user_name** - имя новой учётной записи
- **new_user_password** - пароль для новой учётной записи. Указывать необходимо в виде хэша, например используя команду:
```bash
mkpasswd --method=sha-512
```

## Роль delete-user

Удаление пользовательской учетной записи.
Доступные для использования переменные в **roles/delete-user/vars/main.yml**:

- **user_name** - имя учётной записи, которую необходимо удалить.

# Использование ролей

**ВАЖНО!!!** Playbook содержит переменную **target** в **hosts** для указания хостов/групп хостов, на которых будут запущены необходимые сценарии
```
hosts:  '{{ target }}'
```
Поэтому при запуске playbook.yml через extra-vars необходимо передать значения переменной **target**:
- `ansible-playbook playbook.yml --extra-var "target=all"` - запуск на всех хостах, указанных в inventory файле.
- `ansible-playbook playbook.yml --extra-var "target=<GROUP_NAME>"` - запуск на определённой группе хостов, указанной в inventory файле.
- `ansible-playbook playbook.yml --extra-var "target=<HOSTNAME>"` - запуск на отдельном хосте, указанном в inventory файле.
- `ansible-playbook playbook.yml --extra-var "target='<HOSTNAME1>, <>HOSTNAME2>'"` - запуск на нескольких отдельных хостах, указанных в inventory файле.

## Tags

Роли в Playbook'e содержат **теги**, по которым можно запускать только указанные сценарии:

- **deploy-tinc-vpn**
- **update-packages**
- **create-user**
- **delete-user**


## Запуск определенных ролей

- `ansible-playbook playbook.yml -t update-packages --extra-var "target=all"` - запуск роли **update-packages** на всех хостах, указанных в inventory файле.
- `ansible-playbook playbook.yml -t create-user --extra-var "target=all"` - запуск роли **create-user** на всех хостах, указанных в inventory файле.
- `ansible-playbook playbook.yml -t update-packages -t create-user --extra-var "target=all"` - запуск ролей **update-packages** и **create-user** на всех хостах, указанных в inventory файле.
- `ansible-playbook playbook.yml -t 'update-packages, create-user' --extra-var "target=all"` - запуск ролей **update-packages** и **create-user** на всех хостах, указанных в inventory файле.

## Немного об Ansible extra-vars

**ВАЖНО!!!** Переменные  переданные  в  **extra-vars**  имеют  наивысший приоритет.

## Использование внешних переменных

Объявление переменной в **extra-vars** может быть осуществлено с помощью ключей:

- `-e`
- `--extra-var`
- `--extra-vars`

Можно передавать сразу несколько переменных и их значений, например:
```bash
ansible-playbook playbook.yml -t 'role1, role2' -e "target='<HOSTNAME1>, <>HOSTNAME2>' ansible_user=<user> ansible_password=<password>"
```
